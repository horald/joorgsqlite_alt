<?php
include("bootstrapfunc.php");
$id=$_GET['id'];
bootstraphead("");
bootstrapbegin("Verzeichnisse","");
echo "<a href='../index.php?id=".$id."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";
echo "<a href='dirlistnew.php?id=".$id."'  class='btn btn-primary btn-sm active' role='button'>Neu</a> ";
echo "<br>";
$dir="../sites/views/";
$adir = scandir($dir);
echo "<table class='table table-hover'>";
echo "<tr>";
echo "<th>Verzeichnis</th>";
echo "<th>Edit</th>";
echo "</tr>";
foreach ($adir as $key => $value) {
  if (!in_array($value,array(".",".."))) {
  	 echo "<tr>";	
    echo "<td>".$value."</td><td><a href='edit.php?id=".$id."&verz=".$value."' class='btn btn-primary btn-sm active' role='button'>edit</a></td>";
    echo "</tr>";
  }
}
echo "</table>";
bootstrapend();
?>